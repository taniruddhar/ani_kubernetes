yum install epel-release wget -y
yum install java-1.8.0-openjdk.x86_64 -y
java -version
cp /etc/profile /etc/profile_backup
echo 'export JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk'| tee -a /etc/profile
echo 'export JRE_HOME=/usr/lib/jvm/jre' | tee -a /etc/profile
tail /etc/profile
source /etc/profile
echo $JAVA_HOME
echo $JRE_HOME
-----------------------------------------------------------------------------------------------------------------------

https://s3-us-west-2.amazonaws.com/studentapi-cit/student.war
https://s3-us-west-2.amazonaws.com/studentapi-cit/mysql-connector.jar
-----------------------------------------------------------------------------------------------------------------------




<Resource name="jdbc/TestDB" auth="Container" type="javax.sql.DataSource"
               maxTotal="100" maxIdle="30" maxWaitMillis="10000"
               username="{{DBUSERNAME}}" password="{{DBPASSWORD}}" driverClassName="com.mysql.jdbc.Driver"
               url="jdbc:mysql://{{DBENDPOINT}}:3306/{{DBNAME}}"/>
------------------------------------------------------------------------------------------------------------------------

use student;
CREATE TABLE if not exists students(student_id INT NOT NULL AUTO_INCREMENT, 
	student_name VARCHAR(100) NOT NULL, 
	student_addr VARCHAR(100) NOT NULL, 
	student_age VARCHAR(3) NOT NULL,
	student_qual VARCHAR(20) NOT NULL,
	student_percent VARCHAR(10) NOT NULL,
	student_year_passed VARCHAR(10) NOT NULL, 
	PRIMARY KEY (student_id)
);
